angular.module( 'minhasDiretivas', [] ).directive( 'validadePassword', function(){
	var PASSWORD_REGEXP = /[^A-Za-z0-9\+\/\=]/g;

	return {
		require: 'ngModel',
		restrict: 'AE',
		link: function( scope, elm, attrs, ctrl ){
	      	if ( ctrl && ctrl.$validators.password ){
		        ctrl.$validators.password = function( modelValue ){
		          return ctrl.$isEmpty( modelValue ) || PASSWORD_REGEXP.test( modelValue );
		        };
	     	}
    	}

	};
});